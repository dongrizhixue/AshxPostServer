﻿using System.Web;

namespace AshxPostServer
{
    /// <summary>
    /// ServerHandler 的摘要说明
    /// </summary>
    public class ServerHandler : IHttpHandler
    {
        
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/x-www-form-urlencoded; charset=UTF-8";
            string data = context.Request["data"];
            context.Response.Write(data);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}